import React from 'react'
import Scrollspy from 'react-scrollspy'
import Scroll from './Scroll'

const Nav = (props) => (
    <nav id="nav" className={props.sticky ? 'alt' : ''}>
        <Scrollspy items={ ['dev', 'terminal', 'messaging', 'misc'] } currentClassName="is-active" offset={-300}>
            <li>
                <Scroll type="id" element="dev">
                    <a href="#">Dev Tools</a>
                </Scroll>
            </li>
            <li>
                <Scroll type="id" element="terminal">
                    <a href="#">Terminal</a>
                </Scroll>
            </li>
            <li>
                <Scroll type="id" element="misc">
                    <a href="#">Other</a>
                </Scroll>
            </li>
        </Scrollspy>
    </nav>
)

export default Nav
