import React from 'react'
import Link from 'gatsby-link'

import logo from '../assets/images/logo-sm.png';

const Header = (props) => (
    <header id="header" className="alt">
        <span className="logo"><img src={logo} alt="" /></span>
        <h1>GitLab Theme</h1>
        <p>Themes for all the things, all inspired by <a href="https://about.gitlab.com">GitLab <i className="icon fa-gitlab"></i></a>.</p>
    </header>
)

export default Header
