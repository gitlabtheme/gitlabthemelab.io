import React from 'react'
import Link from 'gatsby-link'

import logo from '../assets/images/logo.svg';

const Footer = (props) => (
    <footer id="footer">
        <section>
            <h2>About</h2>
            <p>
            This is just a fun project from <a href="https://twitter.com/olearycrew">olearycrew</a> and <a href="https://twitter.com/tipyn2903">tipyn</a> to collect some GitLab inspired themes in one place.  
            We're always looking for help - feel free to contribute; however you can.  
            Open an issue, link to something you like, point out a problem, or fork the repo and contribute directly!
            </p>
            <ul className="actions">
                <li><Link to="https://gitlab.com/gitlabtheme/gitlabtheme.gitlab.io" className="button">Contribute</Link></li>
            </ul>
        </section>
        <p className="copyright">&copy; <a href="https://gitlab.com/gitlabtheme">GitLab Theme</a>. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
    </footer>
)

export default Footer
