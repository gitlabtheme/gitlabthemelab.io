import React from 'react'
import Link from 'gatsby-link'

const Themes = (props) => (
    <section id={props.theid} className={props.classes}>
        <header className="major">
            <h2>Miscellaneous</h2>
        </header>
        <ul className="features">
            <li>
            <span className="icon major style1 fa-code"></span>
            <h3>Chrome</h3>
            <p>
                Google Chrome is a freeware web browser developed by Google
                <br/>
                <a href="https://chrome.google.com/webstore/detail/gitlab-dark-theme/lmhnpgogldejgkbmmenhhkfanfhbdogo" target="_blank">Get the theme here</a>
            </p>
            </li>
            <li>
            <span className="icon major style3 fa-slack"></span>
            <h3>Slack</h3>
            <p>
                <code>
                #643685,#634489,#FC6D26,#ffffff,
                #71558f,#ffffff,#FCA326,#e24329
                </code>
            </p>
            </li>
        </ul>
    </section>
)

export default Themes