import React from 'react'
import Link from 'gatsby-link'

const Themes = (props) => (
    <section id={props.theid} className={props.classes}>
        <header className="major">
            <h2>Messaging</h2>
        </header>
        <ul className="features">
            <li>
            <span className="icon major style3 fa-slack"></span>
            <h3>Slack</h3>
            <p>
                <code>
                #643685,#634489,#FC6D26,#ffffff,
                #71558f,#ffffff,#FCA326,#e24329
                </code>
            </p>
            </li>
        </ul>
    </section>
)

export default Themes