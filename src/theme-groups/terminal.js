import React from 'react'
import Link from 'gatsby-link'

const Themes = (props) => (
    <section id={props.theid} className={props.classes}>
        <header className="major">
            <h2>Terminal</h2>
        </header>
        <ul className="features">
            <li>
            <span className="icon major style5 fa-desktop"></span>
            <h3>Terminal.app</h3>
            <p>
                There are four variants available.
                They include: &nbsp;
                <a href="https://gitlab.com/gitlabtheme/gitlabtheme.gitlab.io/raw/master/themes/mac-terminal/gitlab.terminal">
                    GitLab
                </a>, &nbsp;
                <a href="https://gitlab.com/gitlabtheme/gitlabtheme.gitlab.io/raw/master/themes/mac-terminal/gitlab-alt.terminal">
                    GitLab Alt
                </a>, &nbsp;
                <a href="https://gitlab.com/gitlabtheme/gitlabtheme.gitlab.io/raw/master/themes/mac-terminal/gitlab-light.terminal">
                    GitLab Light
                </a> and, &nbsp;
                <a href="https://gitlab.com/gitlabtheme/gitlabtheme.gitlab.io/raw/master/themes/mac-terminal/gitlab-dark.terminal">
                    Gitlab Dark
                </a>.
            </p>
            </li>
            <li>
            <span className="icon major style1 fa-terminal"></span>
            <h3>iTerm (WIP)</h3>
            <p>
                Coming soon
            </p>
            </li>
            <li>
            <span className="icon major style3 fa-bookmark"></span>
            <h3>zsh (WIP)</h3>
            <p>
                Coming soon
            </p>
            </li>
        </ul>
    </section>
)

export default Themes