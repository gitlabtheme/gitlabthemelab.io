import React from 'react'
import Link from 'gatsby-link'

const Themes = (props) => (
    <section id={props.theid} className={props.classes}>
        <header className="major">
            <h2>Dev Tools</h2>
        </header>
        <ul className="features">
            <li>
            <span className="icon major style1 fa-code"></span>
            <h3>VS Code</h3>
            <p>
                Visual Studio Code is an open source multi-platform IDE.
                <br/>
                <a href="https://gitlab.com/gitlabtheme/vscode" target="_blank">Get the theme here</a>
            </p>
            </li>
            <li>
            <span className="icon major style3 fa-copy"></span>
            <h3>Vim (WIP)</h3>
            <p>
                Coming soon
            </p>
            </li>
            <li>
            <span className="icon major style5 fa-align-right"></span>
            <h3>Sublime (WIP)</h3>
            <p>
            Coming soon
            </p>
            </li>
            <li>
            <span className="icon major style4 fa-file"></span>
            <h3>Emacs (WIP)</h3>
            <p>
                Coming soon
            </p>
            </li>
        </ul>
    </section>
)

export default Themes